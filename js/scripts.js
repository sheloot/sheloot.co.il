(function($){
	$(document).ready(function() {	

		// Scroll to Top
		jQuery('.scrolltotop').click(function(){
			jQuery('html').animate({'scrollTop' : '0px'}, 400);
			return false;
		});
		
		jQuery(window).scroll(function(){
			var upto = jQuery(window).scrollTop();
			if(upto > 500) {
				jQuery('.scrolltotop').fadeIn();
			} else {
				jQuery('.scrolltotop').fadeOut();
			}
		});

		$('.owl-carousel').owlCarousel({
          	items: 5, 
            margin: 10, 
            autoplay: true,               
            dots: false,              
            loop: true,
            responsive: {
              0: {
                items: 3
              },
              768: {
                items: 5
              },
              1200: {
                items: 10
              }
            }
		 })		
		 
		new WOW().init();
		
		
	});
})(jQuery);